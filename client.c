/*
** client.c -- a stream socket client demo
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <arpa/inet.h>

#define PORT "3494" // Porta que os usuários irão usar para se conectar

#define MAXDATASIZE 2000 // Tamanho máximo da mensagem que será enviada para o cliente

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


#define STRING_SIZE 50 // Número máximo de caracteres em cada campo do Perfil

#define STRING_SIZE_DOUBLED 101

#define ARRAY_SIZE 5 // Número máximo de entradas para Habilidades e Experiência

// Constantes auxiliares da função getLine()
#define OK 0
#define NO_INPUT 1
#define TOO_LONG 2

// Estrutura de dados de um perfil
struct perfil {
  char name[STRING_SIZE];
  char surname[STRING_SIZE];
  char location[STRING_SIZE];
  char degree[STRING_SIZE];
  char year[STRING_SIZE];
  char email[STRING_SIZE];  
  char skills[ARRAY_SIZE][STRING_SIZE_DOUBLED];
  char experience[ARRAY_SIZE][STRING_SIZE_DOUBLED];
};

typedef struct perfil perfil;

// Função usada para processar as linhas recebidas do usuário
static int getLine (char *prmpt, char *buff, size_t sz) {
    int ch, extra;

    // Get line with buffer overrun protection.
    if (prmpt != NULL) {
        printf ("%s", prmpt);
        fflush (stdout);
    }
    if (fgets (buff, sz, stdin) == NULL)
        return NO_INPUT;

    // If it was too long, there'll be no newline. In that case, we flush
    // to end of line so that excess doesn't affect the next call.
    if (buff[strlen(buff)-1] != '\n') {
        extra = 0;
        while (((ch = getchar()) != '\n') && (ch != EOF))
            extra = 1;
        return (extra == 1) ? TOO_LONG : OK;
    }

    // Otherwise remove newline and give string back to caller.
    buff[strlen(buff)-1] = '\0';
    return OK;
}

// Utilizada pela operação de criação de perfil. Fornece os campos que devem ser preenchidos pelo usuário
void userPrompt(perfil *perf) {
  int loopFlag = 1;
  int loopCounter = 0;
  int getLineValue = 0;
  char loopTemp[STRING_SIZE_DOUBLED];


  getLine("Nome: ", perf->name, STRING_SIZE);
  getLine("Sobrenome: ", perf->surname, STRING_SIZE);
  getLine("Residência: ", perf->location, STRING_SIZE);
  getLine("Formação Acadêmica: ", perf->degree, STRING_SIZE);
  getLine("Ano de Formatura: ", perf->year, STRING_SIZE);
  getLine("Email: ", perf->email, STRING_SIZE);

  printf("Enumere suas habilidades. Você pode especificar até 5 habilidades diferentes. Caso tenha terminado, aperte Enter.\n");
  while(loopFlag) {
      getLineValue = getLine("Habilidade: ", loopTemp, STRING_SIZE);
      if(strlen(loopTemp) == 0 || loopCounter >= ARRAY_SIZE) {
        if(loopCounter >= ARRAY_SIZE)
          printf("Você atingiu o número máximo de Habilidades.\n");
          loopFlag = 0;
      } else {
        if(getLineValue == 0) {
          strcpy(perf->skills[loopCounter], loopTemp);
          loopCounter++;
        } else {
          printf("Você inseriu mais caracteres do que o número permitido de %d caracteres.", STRING_SIZE_DOUBLED);
        } 
      }
    memset(loopTemp,0,sizeof(loopTemp));
  }

  loopFlag = 1;
  loopCounter = 0;

  printf("Fale brevemente sobre suas experiências. Você pode especificar até 5 experiências diferentes. Caso tenha terminado, aperte Enter.\n");
  while(loopFlag) {
      getLineValue = getLine("Experiência: ", loopTemp, STRING_SIZE);
      if(strlen(loopTemp) == 0 || loopCounter >= ARRAY_SIZE) {
        if(loopCounter >= ARRAY_SIZE)
          printf("Você atingiu o número máximo de Habilidades.\n");
          loopFlag = 0;
      } else {
        if(getLineValue == 0) {
          strcpy(perf->experience[loopCounter], loopTemp);
          loopCounter++;
        } else {
          printf("Você inseriu mais caracteres do que o número permitido de %d caracteres.", STRING_SIZE_DOUBLED);
        } 
      }
    memset(loopTemp,0,sizeof(loopTemp));
  }
}

// Serializa o perfil do usuário
void serializeMessage(perfil *perf, char *message, char *operation) {
  int i;

  strcpy(message, operation);
  strcat(message, "\n");
  strcat(message, perf->name);
  strcat(message, "\n");
  strcat(message, perf->surname);
  strcat(message, "\n");
  strcat(message, perf->location);
  strcat(message, "\n");
  strcat(message, perf->degree);
  strcat(message, "\n");
  strcat(message, perf->year);
  strcat(message, "\n");
  strcat(message, perf->email);
  strcat(message, "\n");

  for(i = 0; i < 5; i++) {
    if(strlen(perf->skills[i]) == 0) {
      strcat(message, "EMPTY\n");
    } else {
      strcat(message, perf->skills[i]);
      strcat(message, "\n");
    }
  }

  for(i = 0; i < 5; i++) {
    if(strlen(perf->experience[i]) == 0) {
      strcat(message, "EMPTY\n");
    } else {
      strcat(message, perf->experience[i]);
      strcat(message, "\n");
    }
  }
}

// Serializa mensagens referentes a operações de listar mensagens
void serializeListOperationsMessage(char *message, char *operation, char *key) {
  strcpy(message, operation);
  strcat(message, "\n");
  strcat(message, key);
  strcat(message, "\n");
}

// Serializa mensagens referentes a operação de adicionar experiências
void serializeAddExperience(char *message, char *operation, char *email, char *experience) {
  strcpy(message, operation);
  strcat(message, "\n");
  strcat(message, email);
  strcat(message, "\n");
  strcat(message, experience);
  strcat(message, "\n");
}

// Deserializa o perfil do usuário recebido do cliente
void deserializeMessage(perfil *perf, char *message) {
  char *word;
  int count = 0;
  int indexSkills = 0;
  int indexExperience = 0;
  
  word = strtok(message, "\n");
  if(word == NULL) {
    printf("%s\n", message);
    puts("\t Não existem separadores na string.");
  }

  while(word) {
    if(count == 0) {
      printf("Número da operação: %s\n", word);
    } else if(count == 1) {
      strcpy(perf->name, word);
    } else if(count == 2) {
      strcpy(perf->surname, word);
    } else if(count == 3) {
      strcpy(perf->location, word);
    } else if(count == 4) {
      strcpy(perf->degree, word);
    } else if(count == 5) {
      strcpy(perf->year, word);
    } else if(count == 6) {
      strcpy(perf->email, word);
    } else if(count <= 11) {
      strcpy(perf->skills[indexSkills++], word);
    } else {
      strcpy(perf->experience[indexExperience++], word);
    }
    word = strtok(NULL, "\n");
    count++;
  }
}

// Inicializa os vetores de skills e experience de um perfil
void initializeArray(perfil *perf) {
  int i;
  for(i = 0; i < 5; i++) {
    strcpy(perf->skills[i], "");
    strcpy(perf->experience[i], "");
  }
}

// Imprime um perfil
void printPerfil(perfil *perf) {
  printf("%s\n", perf->name);
  printf("%s\n", perf->surname);
  printf("%s\n", perf->location);
  printf("%s\n", perf->degree);
  printf("%s\n", perf->year);
  printf("%s\n", perf->email);

  int i;
  for(i = 0; i < 5; i++) {
    printf("%s\n", perf->skills[i]);;
  }

  for(i = 0; i < 5; i++) {
    printf("%s\n", perf->experience[i]);
  }
}

// Apresenta as opções disponíveis para o usuário
void printOptions() {
  printf("1. Cadastrar um novo perfil\n");
  printf("2. Acrescentar uma nova experiência profissional em um perfil\n");
  printf("3. Listar pessoas formadas em um determinado curso\n");
  printf("4. Listar pessoas que possuam uma determinada habilidade\n");
  printf("5. Listar pessoas formadas em um determinado ano\n");
  printf("6. Listar todos os perfis\n");
  printf("7. Retornar as informações de um perfil a partir do email.\n");
  printf("8. Remover um perfil a partir do email.\n");
  printf("9. Encerrar a conexão.\n");
}


int main(int argc, char *argv[])
{
	int sockfd, numbytes;  
	char buf[MAXDATASIZE];
	struct addrinfo hints, *servinfo, *p;
	int rv;
	char s[INET6_ADDRSTRLEN];

  // Variáveis auxiliares
	perfil perfEntrada, perfSaida;
  initializeArray(&perfEntrada); //Inicializa os vetores de Habilidades e Experiência
  initializeArray(&perfSaida); // Inicializa os vetores de Habilidades e Experiência
  // string com a mensagem que será enviada para o servidor
  char message[MAXDATASIZE] = "";

	if (argc != 2) {
	    fprintf(stderr,"Inclua o hostname na chamada do programa.\nExemplo: ./client <hostname>\n");
	    exit(1);
	}

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(argv[1], PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("client: socket");
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			perror("client: connect");
			close(sockfd);
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		return 2;
	}

	inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr), s, sizeof s);
	printf("Conectando a %s...\n", s);

	freeaddrinfo(servinfo);

  // Mostra as opções de operações disponíveis ao usuário
  printf("Digite o número correspondente de uma das operações abaixo para executá-la:\n");
  printOptions();

  // Variável que armazena a opção atual
  int option;
  // Loop de operações. Para cada operação, chama as funções necessárias, processa os dados,
  // serializa a mensagem e envia para o servidor. Espera pela resposta do servidor antes de continuar.
  while(1) {
    printf("Digite o número da operação: ");
    scanf("%d", &option);
    getchar();
    memset(message,0,sizeof(message));

    if(option == 1) {
      userPrompt(&perfEntrada);
      serializeMessage(&perfEntrada, message, "1");
      printf("%c\n", message[0]);

      if (send(sockfd, message, strlen(message), 0) < 0) {
        printf("Erro send\n");
        perror("send");
        exit (1);
      }

      if ((numbytes = recv(sockfd, buf, MAXDATASIZE, 0)) < 0) {
        printf("Erro recv\n");
        perror("recv");
        exit(1);
      }

      buf[numbytes] = '\0';

      printf("Mensagem do Servidor: \n%s\n",buf);

    } else if(option == 2) {
      char emailAux[STRING_SIZE], experienceAux[STRING_SIZE];
      printf("Digite o email do perfil que gostaria de adicionar uma nova experiência: ");
      fgets(emailAux, STRING_SIZE, stdin);
      emailAux[strcspn(emailAux, "\n")] = 0;
      printf("Digite a experiência: ");
      fgets(experienceAux, STRING_SIZE, stdin);
      experienceAux[strcspn(experienceAux, "\n")] = 0;
      serializeAddExperience(message, "2", emailAux, experienceAux);

      if (send(sockfd, message, strlen(message), 0) < 0) {
        printf("Erro send\n");
        perror("send");
        exit (1);
      }

      if ((numbytes = recv(sockfd, buf, MAXDATASIZE, 0)) < 0) {
        printf("Erro recv\n");
        perror("recv");
        exit(1);
      }

      buf[numbytes] = '\0';

      printf("Mensagem do Servidor: \n%s\n",buf);

    } else if(option == 3) {
      char degreeAux[STRING_SIZE];
      printf("Digite o nome do Curso que gostaria de buscar: ");
      scanf("%s", degreeAux);
      serializeListOperationsMessage(message, "3", degreeAux);

      printf("%c\n", message[0]);

      if (send(sockfd, message, strlen(message), 0) < 0) {
        printf("Erro send\n");
        perror("send");
        exit (1);
      }

      if ((numbytes = recv(sockfd, buf, MAXDATASIZE, 0)) < 0) {
        printf("Erro recv\n");
        perror("recv");
        exit(1);
      }

      buf[numbytes] = '\0';

      printf("Mensagem do Servidor: \n%s\n",buf);

    } else if(option == 4) {
      char skillAux[STRING_SIZE];
      printf("Digite a habilidade que gostaria de buscar: ");
      scanf("%s", skillAux);
      serializeListOperationsMessage(message, "4", skillAux);

      printf("%c\n", message[0]);

      if (send(sockfd, message, strlen(message), 0) < 0) {
        printf("Erro send\n");
        perror("send");
        exit (1);
      }

      if ((numbytes = recv(sockfd, buf, MAXDATASIZE, 0)) < 0) {
        printf("Erro recv\n");
        perror("recv");
        exit(1);
      }

      buf[numbytes] = '\0';

      printf("Mensagem do Servidor: \n%s\n",buf);
      
    } else if(option == 5) {
      char yearAux[STRING_SIZE];
      printf("Digite o ano que gostaria de buscar: ");
      scanf("%s", yearAux);
      serializeListOperationsMessage(message, "5", yearAux);

      printf("%c\n", message[0]);

      if (send(sockfd, message, strlen(message), 0) < 0) {
        printf("Erro send\n");
        perror("send");
        exit (1);
      }

      if ((numbytes = recv(sockfd, buf, MAXDATASIZE, 0)) < 0) {
        printf("Erro recv\n");
        perror("recv");
        exit(1);
      }

      buf[numbytes] = '\0';

      printf("Mensagem do Servidor: \n%s\n",buf);
      
    } else if(option == 6) {
      serializeListOperationsMessage(message, "6", "LISTALL");

      printf("%c\n", message[0]);

      if (send(sockfd, message, strlen(message), 0) < 0) {
        printf("Erro send\n");
        perror("send");
        exit (1);
      }

      if ((numbytes = recv(sockfd, buf, MAXDATASIZE, 0)) < 0) {
        printf("Erro recv\n");
        perror("recv");
        exit(1);
      }

      buf[numbytes] = '\0';

      printf("Mensagem do Servidor: \n%s\n",buf);
      
    } else if(option == 7) {
      char emailAux[STRING_SIZE];
      printf("Digite o email do perfil que gostaria de buscar: ");
      scanf("%s", emailAux);
      serializeListOperationsMessage(message, "7", emailAux);

      printf("%c\n", message[0]);

      if (send(sockfd, message, strlen(message), 0) < 0) {
        printf("Erro send\n");
        perror("send");
        exit (1);
      }

      if ((numbytes = recv(sockfd, buf, MAXDATASIZE, 0)) < 0) {
        printf("Erro recv\n");
        perror("recv");
        exit(1);
      }

      buf[numbytes] = '\0';

      printf("Mensagem do Servidor: \n%s\n",buf);
      
      
    } else if(option == 8) {
      char removeEmailAux[STRING_SIZE];
      printf("Digite o email do perfil que gostaria de remover: ");
      scanf("%s", removeEmailAux);
      serializeListOperationsMessage(message, "8", removeEmailAux);

      printf("%c\n", message[0]);

      if (send(sockfd, message, strlen(message), 0) < 0) {
        printf("Erro send\n");
        perror("send");
        exit (1);
      }

      if ((numbytes = recv(sockfd, buf, MAXDATASIZE, 0)) < 0) {
        printf("Erro recv\n");
        perror("recv");
        exit(1);
      }

      buf[numbytes] = '\0';

      printf("Mensagem do Servidor: \n%s\n",buf);
      
    } else if(option == 9) {
      if (send(sockfd, "9", 1, 0) < 0) {
        printf("Erro send\n");
        perror("send");
        exit (1);
      }

      if ((numbytes = recv(sockfd, buf, MAXDATASIZE, 0)) < 0) {
        printf("Erro recv\n");
        perror("recv");
        exit(1);
      }

      buf[numbytes] = '\0';
      
      printf("%s\n", buf);

      exit(0);
    } else {
      printf("Opção Inválida.\n");
    }
  }

	close(sockfd);
	return 0;
}
