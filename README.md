# Projeto 1 - Socket TCP - MC833
Projeto desenvolvido pelo aluno Víctor Coelho (RA: 148128) para a disciplina MC833 - Programação de Redes de Computadores durante o Primeiro Semestre de 2021.
## Instruções de Compilação e Execução
Em primeiro lugar, garanta que existam os arquivos **emaiList.txt** e **fileList.txt** criados e vazios. Eles já existem por padrão neste repositório. Os arquivos **client.c** e **server.c** podem ser compilados usando em um ambiente Linux usando um terminal e o compilador GCC:

    gcc server.c -o server
    gcc client.c -o client
E podem ser executados a partir de um terminal pelos comandos:
  
    ./server
    ./client localhost
A aplicação funciona apenas localmente - é por isso que existe a adição de **localhost** como o endereço do servidor na aplicação do cliente. Portanto, abra dois terminais e execute o server e o client em cada um deles. Rode a aplicação do servidor antes de iniciar a do cliente.
Ao iniciar a aplicação do cliente, você verá as seguintes informações:

> ./client localhost
> Conectando a 127.0.0.1...
> 
> Digite o número correspondente de uma das operações abaixo para executá-la:
> 1. Cadastrar um novo perfil
> 2. Acrescentar uma nova experiência profissional em um perfil
> 3. Listar pessoas formadas em um determinado curso
> 4. Listar pessoas que possuam uma determinada habilidade
> 5. Listar pessoas formadas em um determinado ano
> 6. Listar todos os perfis
> 7. Retornar as informações de um perfil a partir do email.
> 8. Remover um perfil a partir do email.
> 9. Encerrar a conexão.
> 
> Digite o número da operação: 

A partir disso, escolha uma das operações e receberá os resultados de cada uma das operações ou, ainda, campos para preencher dados específicos que forem necessários.
## Vídeo de Demonstração de Uso