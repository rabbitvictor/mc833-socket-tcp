#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

#define PORT "3494"  // Porta que os usuários irão usar para se conectar

#define BACKLOG 10	 // Número de conexões que podem ser enfileiradas

#define STRING_SIZE 50 // Número máximo de caracteres em cada campo do Perfil

#define STRING_SIZE_DOUBLED 101

#define ARRAY_SIZE 5 // Número máximo de entradas para Habilidades e Experiência

#define MAXDATASIZE 2000 // Tamanho máximo da mensagem que será enviada para o cliente

void sigchld_handler(int s)
{
	(void)s;

	int saved_errno = errno;

	while(waitpid(-1, NULL, WNOHANG) > 0);

	errno = saved_errno;
}


// Descobre se usaremos IPv4 ou IPv6
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

// Estrutura de dados de um perfil
struct perfil {
  char name[STRING_SIZE];
  char surname[STRING_SIZE];
  char location[STRING_SIZE];
  char degree[STRING_SIZE];
  char year[STRING_SIZE];
  char email[STRING_SIZE];  
  char skills[ARRAY_SIZE][STRING_SIZE_DOUBLED];
  char experience[ARRAY_SIZE][STRING_SIZE_DOUBLED];
};

typedef struct perfil perfil;

// Serializa o perfil do usuário
void serializeMessage(perfil *perf, char *message, char *operation) {
  int i;
  if(strcmp(operation, "1") == 0) {
    printf("Entrou na Serialização Operação 1\n");
    strcpy(message, operation);
    strcat(message, "\n");
    strcat(message, perf->name);
    strcat(message, "\n");
    strcat(message, perf->surname);
    strcat(message, "\n");
    strcat(message, perf->location);
    strcat(message, "\n");
    strcat(message, perf->degree);
    strcat(message, "\n");
    strcat(message, perf->year);
    strcat(message, "\n");
    strcat(message, perf->email);
    strcat(message, "\n");

    for(i = 0; i < 5; i++) {
      if(strlen(perf->skills[i]) == 0) {
        strcat(message, "EMPTY\n");
      } else {
        strcat(message, perf->skills[i]);
        strcat(message, "\n");
      }
    }

    for(i = 0; i < 5; i++) {
      if(strlen(perf->experience[i]) == 0) {
        strcat(message, "EMPTY\n");
      } else {
        strcat(message, perf->experience[i]);
        strcat(message, "\n");
      }
    }
  }
}

// Deserializa o perfil do usuário recebido do cliente
void deserializeMessage(perfil *perf, char *buffer) {
  char *word;
  int count = 0;
  int indexSkills = 0;
  int indexExperience = 0;
  
  word = strtok(buffer, "\n");
  if(word == NULL) {
    printf("%s\n", buffer);
    puts("\t Não existem separadores na string.");
  }

  while(word) {
    if(count == 0) {
      printf("Número da operação: %s\n", word);
    } else if(count == 1) {
      strcpy(perf->name, word);
    } else if(count == 2) {
      strcpy(perf->surname, word);
    } else if(count == 3) {
      strcpy(perf->location, word);
    } else if(count == 4) {
      strcpy(perf->degree, word);
    } else if(count == 5) {
      strcpy(perf->year, word);
    } else if(count == 6) {
      strcpy(perf->email, word);
    } else if(count <= 11) {
      strcpy(perf->skills[indexSkills++], word);
    } else {
      strcpy(perf->experience[indexExperience++], word);
    }
    word = strtok(NULL, "\n");
    count++;
  }
}

// Deserializa mensagens recebidas do cliente para operações de listar mensagens
void deserializeListOperationsMessage(char *buffer, char *key, char *operation) {
	char *word;
  int count = 0;
	word = strtok(buffer, "\n");
  if(word == NULL) {
    printf("%s\n", buffer);
    puts("\t Não existem separadores na string.");
  }

	while(word) {
		if(count == 0) {
			printf("Número da Operação: %s\n", word);
		} else if(count == 1) {
			strcpy(key, word);
		}
		word = strtok(NULL, "\n");
		count++;
	}
}

// Deserializa mensagens recebidas do cliente para a operação de adicionar experiências a um perfil
void deserializeAddExperience(char *buffer, char *email, char *experience) {
		char *word;
  int count = 0;
	word = strtok(buffer, "\n");
  if(word == NULL) {
    printf("%s\n", buffer);
    puts("\t Não existem separadores na string.");
  }

	while(word) {
		if(count == 0) {
			printf("Número da Operação: %s\n", word);
		} else if(count == 1) {
			strcpy(email, word);
		} else if(count == 2) {
			strcpy(experience, word);
		}
		word = strtok(NULL, "\n");
		count++;
	}
}

// Adiciona uma linha a um arquivo
void fileAppend(char *line, char *fileName) {
  FILE *fptr;

	fptr = fopen(fileName, "a");
	if(fptr == NULL) {
		printf("File Open Error!\n");
		exit(1);
	}

  fprintf(fptr, "%s\n", line);

  fclose(fptr);

}

// Cria o arquivo que armazena um perfil recebido de um cliente na operação de adicionar perfil.
void fileHandler(perfil *perf, char *operation) {
	FILE *fptr;
	char fileName[STRING_SIZE_DOUBLED];
	if(strcmp(operation, "1") == 0) {
		strcpy(fileName, perf->name);
		strcat(fileName, perf->surname);
		strcat(fileName, ".txt");

		fptr = fopen(fileName, "w");
		if(fptr == NULL) {
			printf("File Open Error!\n");
			exit(1);
		}

		fprintf(fptr, "%s\n", perf->name);
		fprintf(fptr, "%s\n", perf->surname);
		fprintf(fptr, "%s\n", perf->location);
		fprintf(fptr, "%s\n", perf->degree);
		fprintf(fptr, "%s\n", perf->year);
		fprintf(fptr, "%s\n", perf->email);

		int i;
		for(i = 0; i < ARRAY_SIZE; i++) {
			fprintf(fptr, "%s\n", perf->skills[i]);
		}

		for(i = 0; i < ARRAY_SIZE; i++) {
			fprintf(fptr, "%s\n", perf->experience[i]);
		}	

		fclose(fptr);

		fileAppend(fileName, "fileList.txt");
	}
}

// Função utilizada para remover uma linha específica de um arquivo. Em específico, remove a linha com o nome do arquivo de perfil que foi removido do arquivo fileList.txt
void recreateFileList(char fileNameList[ARRAY_SIZE][STRING_SIZE], int index, char *rmFileName) {
	FILE *fptr;
	int i;
	if(remove("fileList.txt") == 0) {
		printf("fileNameList.txt antiga removida com sucesso\n");
		fptr = fopen("fileList.txt", "w");
		if(fptr == NULL) {
			printf("Erro ao abrir arquivo.\n");
			exit(1);
		}

		for(i = 0; i < index; i++) {
			if(strcmp(fileNameList[i], rmFileName) != 0) {
				fprintf(fptr, "%s\n", fileNameList[i]);
			}
		}

		fclose(fptr);
		printf("fileNameList.txt atualizada com sucesso.\n");
	}
}

// Função utilizada para remover uma linha específica de um arquivo. Em específico, remove a linha com o email de um perfil que foi removido do arquivo emailList.txt
void recreateEmailList(char emailList[ARRAY_SIZE][STRING_SIZE], int index, char *rmEmail) {
	FILE *fptr;
	int i;
	if(remove("emailList.txt") == 0) {
		printf("emaiList.txt antiga removida com sucesso\n");
		fptr = fopen("emailList.txt", "w");
		if(fptr == NULL) {
			printf("Erro ao abrir arquivo.\n");
			exit(1);
		}

		for(i = 0; i < index; i++) {
			if(strcmp(emailList[i], rmEmail) != 0) {
				fprintf(fptr, "%s\n", emailList[i]);
			}
		}

		fclose(fptr);

		printf("emailList.txt atualizada com sucesso.\n");
	}
}

// Descobre se existe um perfil registrado com o email especificado.
int checkEmailIdentifier(char *email) {
	FILE * fp;
	char * line = NULL;
	size_t len = 0;
	ssize_t read;
	char emailTest[STRING_SIZE + 1];
	strcpy(emailTest, email);
	strcat(emailTest, "\n");
	fp = fopen("emailList.txt", "r");
	if (fp == NULL)
			exit(1);

	while ((read = getline(&line, &len, fp)) != -1) {
		if(strcmp(line, email) == 0) {
			printf("Já existe um perfil com este email.\n");
			return 0;
		}
	}

	fclose(fp);
	if (line)
		free(line);
	printf("Não existe perfil com este email.\n");
	return 1;
}

// Realiza uma busca pelo curso especificado entre todos os arquivos de perfis. Escreve em message as informações de nome e email dos perfis que contém o curso especificado.
void searchByDegree(char *buffer, char *message) {
	FILE *fileList, *perfil;
	char *line = NULL, *lineP = NULL;
	size_t len = 0, lenP = 0;
	ssize_t read, readP;
	int counter = 1, bool = 0, index = 0, i;
	char nomeAux[STRING_SIZE], emailAux[STRING_SIZE];
	char degreeAux[STRING_SIZE + 1];
	char messageAux[MAXDATASIZE] = "";
	char fileNameList[ARRAY_SIZE][STRING_SIZE];
	deserializeListOperationsMessage(buffer, degreeAux, "3");
	printf("Degree: %s", degreeAux);
	printf("Tamanho %s: %ld\n", degreeAux, strlen(degreeAux));

	fileList = fopen("fileList.txt", "r");
	if (fileList == NULL)
			exit(1);

	while ((read = getline(&line, &len, fileList)) != -1) {
		// Remove o \n do final da linha lida
		line[strcspn(line, "\n")] = 0;
		strcpy(fileNameList[index++], line);
	}
	
	fclose(fileList);
	if (line) {
		free(line);
	}

	for(i = 0; i < index; i++) {

		perfil = fopen(fileNameList[i], "r");
		if (perfil == NULL) {
			exit(1);
		}
		while((readP = getline(&lineP, &lenP, perfil)) != -1) {
			lineP[strcspn(lineP, "\n")] = 0; // Remove o \n do final da linha

			if(counter == 1) {
				strcpy(nomeAux, lineP);
			}
			
			if(counter == 4) {
				if(strcmp(lineP, degreeAux) == 0) {
					bool = 1; 
				}
			}
			
			if(counter == 6 && bool == 1) {
				strcpy(emailAux, lineP);
				strcat(message, nomeAux);
				strcat(message, ", ");
				strcat(message, emailAux);
				strcat(message, "\n");
			}
			counter++;
		}

		// Reseta contadores e variáveis auxiliares
		counter = 1;
		bool = 0;
		memset(nomeAux,0,sizeof(nomeAux));
		memset(emailAux,0,sizeof(emailAux));
		fclose(perfil);
		if (lineP) {
			free(lineP);
		}
	}
}

// Realiza uma busca pela habilidade especificada entre todos os arquivos de perfis. Escreve em message as informações de nome e email dos perfis que contém a habilidade especificada.
void searchBySkill(char *buffer, char *message) {
	FILE *fileList, *perfil;
	char *line = NULL, *lineP = NULL;
	size_t len = 0, lenP = 0;
	ssize_t read, readP;
	int counter = 1, bool = 0, index = 0, i;
	char nomeAux[STRING_SIZE], emailAux[STRING_SIZE];
	char skillAux[STRING_SIZE + 1];
	char messageAux[MAXDATASIZE] = "";
	char fileNameList[ARRAY_SIZE][STRING_SIZE];
	deserializeListOperationsMessage(buffer, skillAux, "3");

	fileList = fopen("fileList.txt", "r");
	if (fileList == NULL)
			exit(1);

	while ((read = getline(&line, &len, fileList)) != -1) {
		// Remove o \n do final da linha lida
		line[strcspn(line, "\n")] = 0;
		strcpy(fileNameList[index++], line);
	}
	
	fclose(fileList);
	if (line) {
		free(line);
	}

	for(i = 0; i < index; i++) {

		perfil = fopen(fileNameList[i], "r");
		if (perfil == NULL) {
			exit(1);
		}
		while((readP = getline(&lineP, &lenP, perfil)) != -1) {
			lineP[strcspn(lineP, "\n")] = 0; // Remove o \n do final da linha

			if(counter == 1) {
				strcpy(nomeAux, lineP);
			}
			
			if(counter == 6) {
				strcpy(emailAux, lineP);
			}

			if(counter >= 7 && counter <= 11) {
				if(strcmp(lineP, skillAux) == 0) {
					bool = 1; 
				}
			}

			if(bool) {
				strcat(message, nomeAux);
				strcat(message, ", ");
				strcat(message, emailAux);
				strcat(message, "\n");
			}
			bool = 0;
			counter++;
		}

		// Reseta contadores e variáveis auxiliares
		counter = 1;
		bool = 0;
		memset(nomeAux,0,sizeof(nomeAux));
		memset(emailAux,0,sizeof(emailAux));
		fclose(perfil);
		if (lineP) {
			free(lineP);
		}
	}
}

// Realiza uma busca pelo ano especificado entre todos os arquivos de perfis. Escreve em message as informações de nome, email e curso dos perfis que contém o ano especificado.
void searchByYear(char *buffer, char *message) {
	FILE *fileList, *perfil;
	char *line = NULL, *lineP = NULL;
	size_t len = 0, lenP = 0;
	ssize_t read, readP;
	int counter = 1, bool = 0, index = 0, i;
	char nomeAux[STRING_SIZE], emailAux[STRING_SIZE], degreeAux[STRING_SIZE];
	char yearAux[STRING_SIZE + 1];
	char messageAux[MAXDATASIZE] = "";
	char fileNameList[ARRAY_SIZE][STRING_SIZE];
	deserializeListOperationsMessage(buffer, yearAux, "3");
	printf("year: %s", yearAux);
	printf("Tamanho %s: %ld\n", yearAux, strlen(yearAux));

	fileList = fopen("fileList.txt", "r");
	if (fileList == NULL)
			exit(1);

	while ((read = getline(&line, &len, fileList)) != -1) {
		// Remove o \n do final da linha lida
		line[strcspn(line, "\n")] = 0;
		strcpy(fileNameList[index++], line);
	}
	
	fclose(fileList);
	if (line) {
		free(line);
	}

	for(i = 0; i < index; i++) {

		perfil = fopen(fileNameList[i], "r");
		if (perfil == NULL) {
			exit(1);
		}
		while((readP = getline(&lineP, &lenP, perfil)) != -1) {
			lineP[strcspn(lineP, "\n")] = 0; // Remove o \n do final da linha

			if(counter == 1) {
				strcpy(nomeAux, lineP);
			}

			if(counter == 4) {
				strcpy(degreeAux, lineP);
			}

				if(counter == 5) {
				if(strcmp(lineP, yearAux) == 0) {
					bool = 1; 
				}
			}
			
			if(counter == 6 && bool == 1) {
				strcpy(emailAux, lineP);
				strcat(message, nomeAux);
				strcat(message, ", ");
				strcat(message, degreeAux);
				strcat(message, ", ");
				strcat(message, emailAux);
				strcat(message, "\n");
			}

			counter++;
		}

		// Reseta contadores e variáveis auxiliares
		counter = 1;
		bool = 0;
		memset(nomeAux,0,sizeof(nomeAux));
		memset(emailAux,0,sizeof(emailAux));
		memset(degreeAux,0,sizeof(degreeAux));
		fclose(perfil);
		if (lineP) {
			free(lineP);
		}
	}
}

// Passa para a message todas as informações de todos os perfis existentes.
void listAllPerfis(char *message) {
	FILE *fileList, *perfil;
	char *line = NULL, *lineP = NULL;
	size_t len = 0, lenP = 0;
	ssize_t read, readP;
	int counter = 1, index = 0, i;
	char fileNameList[ARRAY_SIZE][STRING_SIZE];

	fileList = fopen("fileList.txt", "r");
	if (fileList == NULL)
			exit(1);

	while ((read = getline(&line, &len, fileList)) != -1) {
		// Remove o \n do final da linha lida
		line[strcspn(line, "\n")] = 0;
		strcpy(fileNameList[index++], line);
	}
	
	fclose(fileList);
	if (line) {
		free(line);
	}

	for(i = 0; i < index; i++) {

		perfil = fopen(fileNameList[i], "r");
		if (perfil == NULL) {
			exit(1);
		}

		while((readP = getline(&lineP, &lenP, perfil)) != -1) {
			lineP[strcspn(lineP, "\n")] = 0; // Remove o \n do final da linha
			if(strcmp(lineP, "EMPTY") != 0) {
				strcat(message, lineP);
				strcat(message, ", ");
			}
		}
		strcat(message, "\n");

		fclose(perfil);
		if (lineP) {
			free(lineP);
		}
	}
}

// Retorna o perfil encontrado de acordo com o email especificado.
int searchByEmail(char *buffer ,char *message) {
	FILE *fileList, *perfil;
	char *line = NULL, *lineP = NULL;
	size_t len = 0, lenP = 0;
	ssize_t read, readP;
	int counter = 1, index = 0, i, bool = 0;
	char fileNameList[ARRAY_SIZE][STRING_SIZE];
	char emailAux[STRING_SIZE];
	char messageAux[MAXDATASIZE] = "";
	deserializeListOperationsMessage(buffer, emailAux, "7");

	fileList = fopen("fileList.txt", "r");
	if (fileList == NULL)
			exit(1);

	while ((read = getline(&line, &len, fileList)) != -1) {
		// Remove o \n do final da linha lida
		line[strcspn(line, "\n")] = 0;
		strcpy(fileNameList[index++], line);
	}
	
	fclose(fileList);
	if (line) {
		free(line);
	}

	for(i = 0; i < index; i++) {

		perfil = fopen(fileNameList[i], "r");
		if (perfil == NULL) {
			exit(1);
		}

		while((readP = getline(&lineP, &lenP, perfil)) != -1) {
			lineP[strcspn(lineP, "\n")] = 0; // Remove o \n do final da linha

			if(counter == 6) {
				if(strcmp(lineP, emailAux) == 0) {
					bool = 1;
				}
			}

			if(strcmp(lineP, "EMPTY") != 0) {
				strcat(messageAux, lineP);
				strcat(messageAux, ", ");
			}
			counter++;
		}
		counter = 1;
		strcat(messageAux, "\n");

		if(bool) {
			strcpy(message, messageAux);
			fclose(perfil);
			if (lineP) {
				free(lineP);
			}
			return bool;
		}

		memset(messageAux, 0, strlen(messageAux));
		fclose(perfil);
		if (lineP) {
			free(lineP);
		}
	}

	return bool;
}

// Remove um perfil de acordo com o email especificado
int removeByEmail(char *buffer) {
	FILE *fileList, *perfil, *emails;
	char *line = NULL, *lineP = NULL, *lineE = NULL;
	size_t len = 0, lenP = 0, lenE = 0;
	ssize_t read, readP, readE;
	int counter = 1, index = 0, i, bool = 0;
	char fileNameList[ARRAY_SIZE][STRING_SIZE];
	char emailList[ARRAY_SIZE][STRING_SIZE];
	int emailListSize, fileNameListSize;
	char emailAux[STRING_SIZE];
	char messageAux[MAXDATASIZE] = "";
	char fileNameAux[STRING_SIZE] = "";
	deserializeListOperationsMessage(buffer, emailAux, "8");


	// Passa para um vetor os nomes de todos os arquivos de perfis existentes
	fileList = fopen("fileList.txt", "r");
	if (fileList == NULL)
			exit(1);

	while ((read = getline(&line, &len, fileList)) != -1) {
		// Remove o \n do final da linha lida
		line[strcspn(line, "\n")] = 0;
		strcpy(fileNameList[index++], line);
	}
	
	fclose(fileList);
	if (line) {
		free(line);
	}

	fileNameListSize = index;

	// Passa para um vetor todos os emails de perfis existentes
	index = 0;

	emails = fopen("emailList.txt", "r");
	if (emails == NULL)
			exit(1);

	while ((read = getline(&lineE, &lenE, emails)) != -1) {
		// Remove o \n do final da linha lida
		lineE[strcspn(lineE, "\n")] = 0;
		strcpy(emailList[index++], lineE);
	}
	
	fclose(emails);
	if (lineE) {
		free(lineE);
	}

	emailListSize = index;

	// Descobre qual arquivo contém o email requisitado
	for(i = 0; i < index; i++) {

		perfil = fopen(fileNameList[i], "r");
		if (perfil == NULL) {
			exit(1);
		}

		while((readP = getline(&lineP, &lenP, perfil)) != -1) {
			lineP[strcspn(lineP, "\n")] = 0; // Remove o \n do final da linha
			if(counter == 6) {
				if(strcmp(lineP, emailAux) == 0) {
					printf("Encontrou o email buscado.\n");
					strcpy(fileNameAux, fileNameList[i]);
					bool = 1;
				}
			}
			counter++;
		}

		counter = 1;

		fclose(perfil);
		if (lineP) {
			free(lineP);
		}
	}

	if(bool) {
		// Deleta o arquivo correspondente
		if(remove(fileNameAux) == 0) {
			printf("Arquivo Removido com Sucesso\n");
			recreateFileList(fileNameList, fileNameListSize, fileNameAux);
			recreateEmailList(emailList, emailListSize, emailAux);
			return bool;
		}
	}

	return bool;
}

// Adiciona a experiência nova para o arquivo especificado por email
int appendExperience(char *buffer) {
	FILE *fileList, *perfil, *emails;
	char *line = NULL, *lineP = NULL;
	size_t len = 0, lenP = 0;
	ssize_t read, readP;
	int counter = 1, index = 0, i, bool = 0;
	char fileNameList[ARRAY_SIZE][STRING_SIZE];
	char emailList[ARRAY_SIZE][STRING_SIZE];
	int emailListSize, fileNameListSize;
	char emailAux[STRING_SIZE], experienceAux[STRING_SIZE];
	char fileNameAux[STRING_SIZE] = "";
	deserializeAddExperience(buffer, emailAux, experienceAux);


	// Passa para um vetor os nomes de todos os arquivos de perfis existentes
	fileList = fopen("fileList.txt", "r");
	if (fileList == NULL)
			exit(1);

	while ((read = getline(&line, &len, fileList)) != -1) {
		// Remove o \n do final da linha lida
		line[strcspn(line, "\n")] = 0;
		strcpy(fileNameList[index++], line);
	}
	
	fclose(fileList);
	if (line) {
		free(line);
	}

	fileNameListSize = index;

	// Descobre qual arquivo contém o email requisitado
	for(i = 0; i < index; i++) {
		printf("Nome do arquivo: %s\n", fileNameList[i]);

		perfil = fopen(fileNameList[i], "r");
		if (perfil == NULL) {
			exit(1);
		}

		while((readP = getline(&lineP, &lenP, perfil)) != -1) {
			lineP[strcspn(lineP, "\n")] = 0; // Remove o \n do final da linha
			printf("Conteúdo da linha: %s\n", lineP);

			if(counter == 6) {
				if(strcmp(lineP, emailAux) == 0) {
					printf("Encontrou o email buscado.\n");
					strcpy(fileNameAux, fileNameList[i]);
					bool = 1;
				}
			}
			counter++;
		}

		fclose(perfil);
		if (lineP) {
			free(lineP);
		}
	}

	if(bool) {
		// Adiciona a experiência no arquivo correspondente
			fileAppend(experienceAux, fileNameAux);
			return bool;
	}

	return bool;
}

// Reseta o perfil
void resetPerfil(perfil *perf) {
	int i;
	strcpy(perf->name, "");
	strcpy(perf->surname, "");
	strcpy(perf->location, "");
	strcpy(perf->degree, "");
	strcpy(perf->year, "");
	strcpy(perf->email, "");
  for(i = 0; i < 5; i++) {
    strcpy(perf->skills[i], "");
    strcpy(perf->experience[i], "");
  }
}

int main(void)
{
	int sockfd, new_fd;  // sock_fd é usado para escutar, e new_fd aceita novas conexões
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size;
	struct sigaction sa;
	int yes=1;
	char s[INET6_ADDRSTRLEN];
	int rv;
	int numbytes;


	// Variáveis auxiliares das operações entre cliente e servidor
	char clientMessage[MAXDATASIZE];
	char buf[MAXDATASIZE];
	memset(clientMessage,0,sizeof(clientMessage));
	memset(buf,0,sizeof(buf));
	perfil perfReceveid;
	memset(&hints, 0, sizeof hints);


	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// procura pelo primeiro resultado válido e usa bind()
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("server: socket");
			continue;
		}

		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
				sizeof(int)) == -1) {
			perror("setsockopt");
			exit(1);
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("server: bind");
			continue;
		}

		break;
	}

	freeaddrinfo(servinfo);

	if (p == NULL)  {
		fprintf(stderr, "server: bind() falhou\n");
		exit(1);
	}

	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

	sa.sa_handler = sigchld_handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &sa, NULL) == -1) {
		perror("sigaction");
		exit(1);
	}

	printf("Esperando por conexões...\n");

	while(1) {  // loop que espera por novas conexões de clientes
		sin_size = sizeof their_addr;
		new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if (new_fd == -1) {
			printf("Erro no accept()\n");
			perror("accept");
			continue;
		}

		inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), s, sizeof s);
		printf("Conectado %s\n", s);

		if (!fork()) { // Cria um novo processo para cada nova conexão
			printf("Entrou no processo novo\n");
			close(sockfd);

			// Para cada conexão, cria um loop esperando por mensagens
			while(1) {
				if ((numbytes = recv(new_fd, buf, MAXDATASIZE, 0)) == -1) {
					perror("recv");
					exit(0);
				}

				printf("Server Receveid:\n%s\n", buf);

				// Para cada operação possível, compara a primeira letra recebida do usuário, que representa o número da operação.

				if(buf[0] == '1') {
					// 1. Cadastrar um novo perfil
					deserializeMessage(&perfReceveid, buf);

					if(checkEmailIdentifier(perfReceveid.email)) {
						fileAppend(perfReceveid.email, "emailList.txt");

						fileHandler(&perfReceveid, "1");

						if (send(new_fd, "Adicionado com sucesso.", 23, 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}


					} else {
						if (send(new_fd, "Email já cadastrado.", 20, 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					}
				} else if(buf[0] == '2') {
					// 2. Acrescentar uma nova experiência profissional em um perfil
					int response_add;
					response_add = appendExperience(buf);
					if(response_add == 0) {
						if (send(new_fd, "Não existe um perfil cadastrado com este email.", 47, 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					} else {
						if (send(new_fd, "Experiência adicionada com sucesso.", 35, 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					}

				} else if(buf[0] == '3') {
					// 3. Listar pessoas formadas em um determinado curso
					searchByDegree(buf, clientMessage);
					printf("SearchByDegree:\n%s", clientMessage);
					if(strlen(clientMessage) == 0) {
						if (send(new_fd, "Não existem perfis com este Curso.", 34, 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					} else {
						if (send(new_fd, clientMessage, strlen(clientMessage), 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					}

				} else if(buf[0] == '4') {
					// 4. Listar pessoas que possuam uma determinada habilidade
					searchBySkill(buf, clientMessage);
					printf("SearchBySkill:\n%s", clientMessage);
					if(strlen(clientMessage) == 0) {
						if (send(new_fd, "Não existem perfis com esta habilidade.", 39, 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					} else {
						if (send(new_fd, clientMessage, strlen(clientMessage), 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					}

				} else if(buf[0] == '5') {
					// 5. Listar pessoas formadas em um determinado ano
					searchByYear(buf, clientMessage);
					printf("SearchByYear:\n%s", clientMessage);
					if(strlen(clientMessage) == 0) {
						if (send(new_fd, "Não existem perfis com este ano de formatura.", 45, 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					} else {
						if (send(new_fd, clientMessage, strlen(clientMessage), 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					}

				} else if(buf[0] == '6') {
					// 6. Listar todos os perfis
					listAllPerfis(clientMessage);
					printf("listAllPerfis:\n%s", clientMessage);

					if(strlen(clientMessage) == 0) {
						if (send(new_fd, "Não existem perfis cadastrados.", 45, 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					} else {
						if (send(new_fd, clientMessage, strlen(clientMessage), 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					}
				
				} else if(buf[0] == '7') {
					// 7. Retornar as informações de um perfil a partir do email.
					int response;
					response = searchByEmail(buf, clientMessage);
					printf("SearchByEmail:\n%s", clientMessage);
					if(response == 0) {
						if (send(new_fd, "Não existe um perfil cadastrado com este email.", 47, 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					} else {
						if (send(new_fd, clientMessage, strlen(clientMessage), 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					}

				} else if(buf[0] == '8') {
					// 8. Remover um perfil a partir do email.
					int response_delete;
					response_delete = removeByEmail(buf);
					if(response_delete == 0) {
						if (send(new_fd, "Não existe um perfil cadastrado com este email.", 47, 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					} else {
						if (send(new_fd, "Perfil deletado com sucesso.", 28, 0) == -1) {
							printf("Erro no Envio\n");
							perror("send");
						}
					}
				} else if(buf[0] == '9') {
					// 9. Encerrar a conexão
					// Fecha o processo criado para este cliente
					if (send(new_fd, "Conexão encerrada.", 18, 0) == -1) {
						printf("Erro no Envio\n");
						perror("send");
					}
					close(new_fd);
					exit(0);
				}
				// Reseta as variáveis auxiliares
				memset(buf,0,sizeof(buf));
				memset(clientMessage,0,sizeof(clientMessage));
				resetPerfil(&perfReceveid);
			}
		}
		close(new_fd);
	}

	return 0;
}
